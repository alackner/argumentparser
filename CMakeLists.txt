cmake_minimum_required(VERSION 3.10.0)
project(ArgumentParser VERSION 0.1.0)

set(CMAKE_CXX_STANDARD 17)
set(CMAKE_EXPORT_COMPILE_COMMANDS true)

option(BUILD_ARGUMENTPARSER_TESTS "Build tests" OFF)

#####################################################################
# ArgumentParser
#####################################################################
add_subdirectory(${CMAKE_CURRENT_SOURCE_DIR}/src)

#####################################################################
# Testing
#####################################################################
if(BUILD_ARGUMENTPARSER_TESTS)
    add_subdirectory(${CMAKE_CURRENT_SOURCE_DIR}/frameworks/googletest-release-1.10.0 ${CMAKE_CURRENT_SOURCE_DIR}/frameworks/googletest-release-1.10.0/build)
    add_subdirectory(${CMAKE_CURRENT_SOURCE_DIR}/test)
endif()